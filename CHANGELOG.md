# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.4.16] - 2024-10-13

# Changed

- Update to EMP plugin version 0.4.26, for EMP toolbox v0.9.0 and HERD v0.4.25 (!85)
- Update to SMASH version 1.0.1 and Serenity toolbox version 0.4.0


## [0.4.15] - 2024-10-03

### Added

- Monitoring data for CERN-B and 16G FireFly modules (!79)
- MGT-FireFly mapping for VU7P SO1 daughter card on Z1.2 (!83)

### Changed

- Update to SMASH version 0.6.14 (!82)
- Update to EMP plugin version 0.4.25, based on HERD v0.4.24 (!78, !81, !84)

### Fixed

- FireFly configuration commands: Name mangling logic updated to resolve Z1.1 bug (!80)


## [0.4.14] - 2024-07-06

### Changed

- Update to EMP plugin version 0.4.23, for HERD v0.4.21 (!77)


## [0.4.13] - 2024-04-22

### Changed

- Update to EMP plugin version 0.4.22 (fixes bug in detection of Tx CSP FW) (!76)


## [0.4.12] - 2024-03-14

### Changed

- Update to EMP plugin version 0.4.20 (hence HERD version 0.4.18) (!73)
- Stop reading FireFly status registers for disabled channels (!72)
- Reset FireFly command: Improve robustness against I2C errors (!71)


## [0.4.11] - 2024-02-25

### Changed

- Update to EMP plugin version 0.4.19 (hence HERD version 0.4.17, based off SWATCH v1.8.4) (!66, !68, !70)
- FireFly configuration & reset commands: Highlight pre-production parts in messages (#14, !69)
- Format code (!67)

### Fixed

- FireFly serial number and firmware numbers corrected (previously swapped) (!65)


## [0.4.10] - 2023-11-23

### Changed

- Update to EMP plugin version 0.4.15 (hence HERD version 0.4.12)
  * Based off SWATCH 1.8.0, which brings improvements relating to property updates


## [0.4.9] - 2023-11-21

### Changed

- Link and slice test FSMs: Moved FireFly configuration to setup transition (30% performance improvement for 2-layer slice test)
- Add Alma8 build to CI pipeline.


## [0.4.8] - 2023-11-05

### Changed

- Add arm64 build to CI pipeline.
- Update to EMP plugin version 0.4.13 (hence HERD version 0.4.11)
  * The HERD update brings: bugfixes to post-action callback and file deletion; and adaptation of CI to k8s runner infrastructure.
  * Beyond this, the EMP plugin update brings a bugfix for the slice test FSM's continue transition, and a command for injecting errors onto backend links.


## [0.4.7] - 2023-08-08

### Added

- Support for defining custom algo class in downstream repos (!56)

### Changed

- Update to EMP plugin version 0.4.9 (hence HERD v0.4.8)


## [0.4.6] - 2023-07-31

### Changed

- Update to EMP plugin version 0.4.6 (hence EMP toolbox v0.8.2, HERD v0.4.6, SWATCH v1.7.3) (!55)


## [0.4.5] - 2023-07-20

### Changed

- FireFly configuration commands: Disable channels connected to ports in loopback (!54)

### Fixed

- Treat frontend FireFlys as absent - a temporary fix to support tests on boards in TIF (!53)


## [0.4.4] - 2023-07-07

### Changed

- Update to EMP plugin version 0.4.4, based on EMP toolbox version 0.8.0. This plugin now only supports EMP v0.8.x firmware (!52)


## [0.4.3] - 2023-07-05

### Changed

- Link & slice test FSMs: Reset FireFly modules in setup transition rather than configure, to speed up testing (!51)


## [0.4.2] - 2023-06-25

### Changed

- Add warning for low FireFly power (#8)
- FireFly configuration commands: Disable all channels on non-selected FireFlys (!48)
- Update to EMP plugin version 0.4.3 (hence HERD v0.4.3 and SWATCH v1.7.2) (!47)
- Add default frontpanel mapping file, to be overridden on each board (!46)

### Fixed

- Update SMASH version 0.6.13, to include RSSI channel mapping fix for beta 12-channel FireFly parts (!50)


## [0.4.1] - 2023-05-23

### Changed

- Update to EMP plugin version 0.4.1


