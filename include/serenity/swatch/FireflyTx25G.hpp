#ifndef __SERENITY_SWATCH_FIREFLYTX25G_HPP__
#define __SERENITY_SWATCH_FIREFLYTX25G_HPP__


#include "serenity/swatch/FireflyBase.hpp"


namespace serenity {
namespace swatch {

class FireflyTx25G : public FireflyBase {
public:
  FireflyTx25G(const std::string& aId, const std::string& aAlias, const ::Element&);
  virtual ~FireflyTx25G();

private:
  void retrieveMetricValues() final;

  void retrieveOutputPropertyValues(Channel& aChannel) final;

  void retrieveOutputMetricValues(Channel& aChannel) final;

  SimpleMetric<float>* mMetricVoltage;

  std::unordered_map<const Channel*, Property<std::string>*> mPropertyState;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyEqualization;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyPolarity;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertySquelch;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyCDR;

  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricLOL, mMetricLaserFault;

  static const std::vector<size_t> kChannels;
};

} // namespace swatch
} // namespace serenity

#endif /* __SERENITY_SWATCH_FIREFLYTX25G_HPP__ */
