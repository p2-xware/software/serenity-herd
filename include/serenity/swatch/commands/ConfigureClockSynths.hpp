#ifndef __SERENTIY_SWATCH_COMMANDS_CONFIGURECLOCKSYNTHS_HPP__
#define __SERENTIY_SWATCH_COMMANDS_CONFIGURECLOCKSYNTHS_HPP__


#include "swatch/action/Command.hpp"


namespace serenity {
namespace swatch {
namespace commands {

class ConfigureClockSynths : public ::swatch::action::Command {
public:
  ConfigureClockSynths(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~ConfigureClockSynths();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace serenity

#endif
