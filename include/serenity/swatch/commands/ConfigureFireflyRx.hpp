#ifndef __SERENTIY_SWATCH_COMMANDS_CONFIGUREFIREFLYRX_HPP__
#define __SERENTIY_SWATCH_COMMANDS_CONFIGUREFIREFLYRX_HPP__


#include "swatch/action/Command.hpp"


namespace serenity {
namespace swatch {
namespace commands {

class ConfigureFireflyRx : public ::swatch::action::Command {
public:
  ConfigureFireflyRx(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~ConfigureFireflyRx();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace serenity

#endif
