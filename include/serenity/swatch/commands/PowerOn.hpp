#ifndef __SERENTIY_SWATCH_COMMANDS_POWERON_HPP__
#define __SERENTIY_SWATCH_COMMANDS_POWERON_HPP__


#include "swatch/action/Command.hpp"

#include "serenity/board.hpp"


namespace serenity {
namespace swatch {
namespace commands {

template <BoardSeries Series>
class PowerOn : public ::swatch::action::Command {
public:
  PowerOn(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~PowerOn();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);

  void runCoreAction(Smash&);

  static const std::string kDescription;
};

} // namespace commands
} // namespace swatch
} // namespace serenity

#endif
