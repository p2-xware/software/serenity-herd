#ifndef __SERENTIY_SWATCH_COMMANDS_CONFIGURECROSSPOINTSWITCH_HPP__
#define __SERENTIY_SWATCH_COMMANDS_CONFIGURECROSSPOINTSWITCH_HPP__


#include "swatch/action/Command.hpp"


namespace serenity {
namespace swatch {
namespace commands {

class ConfigureCrosspointSwitch : public ::swatch::action::Command {
public:
  ConfigureCrosspointSwitch(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~ConfigureCrosspointSwitch();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace serenity

#endif
