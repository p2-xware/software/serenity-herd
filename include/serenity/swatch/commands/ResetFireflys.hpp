#ifndef __SERENTIY_SWATCH_COMMANDS_RESETFIREFLYS_HPP__
#define __SERENTIY_SWATCH_COMMANDS_RESETFIREFLYS_HPP__


#include "swatch/action/Command.hpp"


namespace serenity {
namespace swatch {
namespace commands {

class ResetFireflys : public ::swatch::action::Command {
public:
  ResetFireflys(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~ResetFireflys();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace serenity

#endif
