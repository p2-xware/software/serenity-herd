#ifndef __SERENTIY_SWATCH_COMMANDS_POWERCYCLE_HPP__
#define __SERENTIY_SWATCH_COMMANDS_POWERCYCLE_HPP__


#include "swatch/action/Command.hpp"

#include "serenity/board.hpp"


namespace serenity {
namespace swatch {
namespace commands {

template <BoardSeries Series>
class PowerCycle : public ::swatch::action::Command {
public:
  PowerCycle(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~PowerCycle();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);

  static const std::string kDescription;
};

} // namespace commands
} // namespace swatch
} // namespace serenity

#endif
