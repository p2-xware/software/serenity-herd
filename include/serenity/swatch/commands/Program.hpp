#ifndef __SERENTIY_SWATCH_COMMANDS_PROGRAM_HPP__
#define __SERENTIY_SWATCH_COMMANDS_PROGRAM_HPP__


#include "emp/swatch/commands/AbstractProgram.hpp"


namespace serenity {
namespace swatch {
namespace commands {

class Program : public emp::swatch::commands::AbstractProgram {
public:
  Program(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~Program();

private:
  void program(const std::string& aProgrammingFilePath);
};

} // namespace commands
} // namespace swatch
} // namespace serenity

#endif
