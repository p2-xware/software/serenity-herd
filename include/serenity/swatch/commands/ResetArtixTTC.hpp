#ifndef __SERENTIY_SWATCH_COMMANDS_RESET_HPP__
#define __SERENTIY_SWATCH_COMMANDS_RESET_HPP__


#include "swatch/action/Command.hpp"


namespace serenity {
namespace swatch {
namespace commands {

class ResetArtixTTC : public ::swatch::action::Command {
public:
  ResetArtixTTC(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~ResetArtixTTC();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace serenity

#endif
