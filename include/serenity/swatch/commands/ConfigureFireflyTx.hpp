#ifndef __SERENTIY_SWATCH_COMMANDS_CONFIGUREFIREFLYTX_HPP__
#define __SERENTIY_SWATCH_COMMANDS_CONFIGUREFIREFLYTX_HPP__


#include "swatch/action/Command.hpp"


namespace serenity {
namespace swatch {
namespace commands {

class ConfigureFireflyTx : public ::swatch::action::Command {
public:
  ConfigureFireflyTx(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~ConfigureFireflyTx();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace serenity

#endif
