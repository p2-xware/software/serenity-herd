
#ifndef __SERENTIY_SWATCH_ARTIX_HPP__
#define __SERENTIY_SWATCH_ARTIX_HPP__


#include <memory>

#include "serenity/ArtixTTCController.hpp"

#include "swatch/phase2/ServiceModule.hpp"


namespace serenity {
namespace swatch {


class Artix : public ::swatch::phase2::ServiceModule {
public:
  Artix(const ::swatch::core::AbstractStub& aStub);
  virtual ~Artix();

  ArtixTTCController& getController();

private:
  void retrieveMetricValues() override;

  std::unique_ptr<ArtixTTCController> mController;

  struct CmdIds {
    static const std::string kReset;
  };
};


} // namespace swatch
} // namespace serenity

#endif
