#ifndef __SERENITY_SWATCH_FireflyTx16G_HPP__
#define __SERENITY_SWATCH_FireflyTx16G_HPP__


#include "serenity/swatch/FireflyBase.hpp"


namespace serenity {
namespace swatch {

// Firefly Tx class for both 16G and CERN-B parts

class FireflyTx16G : public FireflyBase {
public:
  FireflyTx16G(const std::string& aId, const std::string& aAlias, const ::Element&);
  virtual ~FireflyTx16G();

private:
  void retrieveMetricValues() final;

  void retrieveOutputPropertyValues(Channel& aChannel) final;

  void retrieveOutputMetricValues(Channel& aChannel) final;

  SimpleMetric<float>* mMetricVoltage;

  std::unordered_map<const Channel*, Property<std::string>*> mPropertyState;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyPolarity;

  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricLaserFault;

  static const std::vector<size_t> kChannels;
};

} // namespace swatch
} // namespace serenity

#endif /* __SERENITY_SWATCH_FireflyTx16G_HPP__ */
