#ifndef __SERENITY_SWATCH_FIREFLYRX25G_HPP__
#define __SERENITY_SWATCH_FIREFLYRX25G_HPP__


#include "serenity/swatch/FireflyBase.hpp"


namespace serenity {
namespace swatch {

class FireflyRx25G : public FireflyBase {
public:
  FireflyRx25G(const std::string& aId, const std::string& aAlias, const ::Element&);
  virtual ~FireflyRx25G();

private:
  void retrieveInputPropertyValues(Channel& aChannel) final;

  void retrieveMetricValues() final;

  void retrieveInputMetricValues(Channel& aChannel) final;

  std::unordered_map<const Channel*, Property<std::string>*> mPropertyState;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyAmplitude;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyPolarity;
  std::unordered_map<const Channel*, Property<float>*> mPropertyPreEmph;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyCDR;

  std::unordered_map<const Channel*, SimpleMetric<float>*> mMetricPower;
  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricLOL, mMetricLOS;

  static const std::vector<size_t> kChannels;
};

} // namespace swatch
} // namespace serenity

#endif /* __SERENITY_SWATCH_FIREFLYRX25G_HPP__ */
