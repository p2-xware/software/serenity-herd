
#ifndef __SERENITY_SWATCH_BASEPROCESSOR_HPP__
#define __SERENITY_SWATCH_BASEPROCESSOR_HPP__


#include "emp/swatch/Processor.hpp"

#include "serenity/IOChannelMap.hpp"
#include "serenity/board.hpp"


namespace serenity {
namespace swatch {

class BaseProcessor : public emp::swatch::Processor {
public:
  // Type carrier struct, required to pass board series to BaseProcessor constructor as a template parameter
  template <BoardSeries Series>
  struct TypeCarrier {
    static const BoardSeries S { Series };
  };

  virtual ~BaseProcessor();

  const std::string& getIOChannelMapFpgaId() const;

  const IOChannelMap& getIOChannelMap() const;

protected:
  template <BoardSeries Series>
  BaseProcessor(const ::swatch::core::AbstractStub&, const IOChannelMap&, const size_t, TypeCarrier<Series>);

private:
  bool checkPresence() const final;

  const IOChannelMap mIOChannelMap;
  const std::string mIOChannelMapFpgaId;

  // STATIC

  static bool extractRxInversion(const IOChannelMap&, const std::string&, const size_t);

  static bool extractTxInversion(const IOChannelMap&, const std::string&, const size_t);

  static boost::optional<::swatch::phase2::ChannelID> extractRxConnection(const IOChannelMap&, const std::string&, const size_t);

  static boost::optional<::swatch::phase2::ChannelID> extractTxConnection(const IOChannelMap&, const std::string&, const size_t);

  struct CmdIds {
    static const std::string kConfigureClockSynths;
    static const std::string kConfigureXpointSwitch;
    static const std::string kConfigureFireflyRx;
    static const std::string kConfigureFireflyTx;
    static const std::string kResetFireflys;
    static const std::string kPowerCycle;
    static const std::string kPowerOff;
    static const std::string kPowerOn;
    static const std::string kProgram;
  };
};

} // namespace swatch
} // namespace serenity

#endif
