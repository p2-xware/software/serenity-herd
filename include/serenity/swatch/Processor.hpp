
#ifndef __SERENITY_SWATCH_PROCESSOR_HPP__
#define __SERENITY_SWATCH_PROCESSOR_HPP__


#include "serenity/board.hpp"

#include "serenity/swatch/BaseProcessor.hpp"


namespace serenity {
namespace swatch {

template <BoardSeries Series>
class Processor;


template <>
class Processor<BoardSeries::S1> : public BaseProcessor {
public:
  Processor(const ::swatch::core::AbstractStub&);
  virtual ~Processor();

private:
  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  SimpleMetric<float>& mPowerMetric;
};


template <>
class Processor<BoardSeries::Z1> : public BaseProcessor {
public:
  Processor(const ::swatch::core::AbstractStub&);
  virtual ~Processor();

  Site getSite() const;

private:
  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  const Site mSite;

  Property<std::string>& mDCType;
  Property<std::string>& mDCUID;

  SimpleMetric<float>* mPowerMetric;

  // STATIC

  static DaughterCardType getDCType(const Site);

  static size_t getNumberOfPorts(const Site);

  // FIXME: Deduce this from IOChannelMap ??
  static const std::map<DaughterCardType, size_t> kPortCountMap;
};


typedef Processor<BoardSeries::S1> S1Processor;
typedef Processor<BoardSeries::Z1> Z1Processor;

} // namespace swatch
} // namespace serenity

#endif
