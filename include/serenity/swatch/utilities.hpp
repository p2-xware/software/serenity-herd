#ifndef __SERENITY_SWATCH_UTILITIES_HPP__
#define __SERENITY_SWATCH_UTILITIES_HPP__


#include <memory>
#include <string>
#include <utility>
#include <vector>


class Element;

namespace swatch {
namespace phase2 {
class OpticalModule;
}
}

namespace serenity {
namespace swatch {

std::pair<std::vector<::swatch::phase2::OpticalModule*>, std::vector<std::string>> createOpticalModules();

const std::string getFireflyFirmwareVersion(Element& lElement, const std::string lName);

const std::string getFireflyBatch(const std::string& firmwareVersion);

} // namespace swatch
} // namespace serenity

#endif /* __SERENITY_SWATCH_UTILITIES_HPP__ */