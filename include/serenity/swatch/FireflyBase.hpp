#ifndef __SERENITY_SWATCH_FIREFLYBASE_HPP__
#define __SERENITY_SWATCH_FIREFLYBASE_HPP__


#include "swatch/phase2/OpticalModule.hpp"


// Forward declare SMASH classes
class Element;

namespace serenity {
namespace swatch {

class FireflyBase : public ::swatch::phase2::OpticalModule {
public:
  FireflyBase(const std::string& aId, const std::string& aAlias, const ::Element&, const std::vector<size_t>& aRxIndices, const std::vector<size_t>& aTxIndices);
  virtual ~FireflyBase();

private:
  bool checkPresence() const final;

  void retrievePropertyValues() final;

  const std::string mPartStr;
  Property<std::string>& mPart;
  Property<std::string>& mSerialNumber;
  Property<std::string>& mFirmwareVersion;

protected:
  // std::pair<T, std::string> measure(Element& , const std::string& );

  static std::pair<float, std::string> measurePhysicalQuantity(Element&, const std::string&);

  static std::map<size_t, std::string> measureChannelSetting(Element&, const std::string&, const std::vector<size_t> aChannels);

  static std::map<size_t, bool> measureChannelFlag(Element&, const std::string&, const std::vector<size_t> aChannels, const std::unordered_map<std::string, bool>& aInterpretationMap);

  static const std::unordered_map<std::string, bool> kAlarmValueMap;
  static const std::unordered_map<std::string, bool> kEnableValueMap;
  static const std::unordered_map<std::string, bool> kPolarityInvertedValueMap;

  const std::string mElementName;

  SimpleMetric<float>& mMetricTemperature;
  SimpleMetric<float>& mMetricPeakTemperature;

  // std::vector<SimpleMetric<float>*> mMetricRxPower;
};

} // namespace swatch
} // namespace serenity

#endif /* __SERENITY_SWATCH_FIREFLYBASE_HPP__ */
