
#ifndef __SERENTIY_SWATCH_ZYNQ_HPP__
#define __SERENTIY_SWATCH_ZYNQ_HPP__


#include "swatch/phase2/ServiceModule.hpp"


namespace serenity {
namespace swatch {


class Zynq : public ::swatch::phase2::ServiceModule {
public:
  Zynq(const ::swatch::core::AbstractStub& aStub);
  virtual ~Zynq();

private:
  void retrieveMetricValues() override;
};


} // namespace swatch
} // namespace serenity

#endif
