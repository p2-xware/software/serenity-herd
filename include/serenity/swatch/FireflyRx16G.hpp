#ifndef __SERENITY_SWATCH_FireflyRx16G_HPP__
#define __SERENITY_SWATCH_FireflyRx16G_HPP__


#include "serenity/swatch/FireflyBase.hpp"


namespace serenity {
namespace swatch {

// Firefly Rx class for both 16G and CERN-B parts

class FireflyRx16G : public FireflyBase {
public:
  FireflyRx16G(const std::string& aId, const std::string& aAlias, const ::Element&);
  virtual ~FireflyRx16G();

private:
  void retrieveInputPropertyValues(Channel& aChannel) final;

  void retrieveMetricValues() final;

  void retrieveInputMetricValues(Channel& aChannel) final;

  std::unordered_map<const Channel*, Property<std::string>*> mPropertyState;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyAmplitude;
  std::unordered_map<const Channel*, Property<std::string>*> mPropertyPolarity;

  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricLOS;

  static const std::vector<size_t> kChannels;
};

} // namespace swatch
} // namespace serenity

#endif /* __SERENITY_SWATCH_FireflyRx16G_HPP__ */
