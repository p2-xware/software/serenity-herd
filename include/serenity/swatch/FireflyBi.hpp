#ifndef __SERENITY_SWATCH_FIREFLYBI_HPP__
#define __SERENITY_SWATCH_FIREFLYBI_HPP__


#include "serenity/swatch/FireflyBase.hpp"


namespace serenity {
namespace swatch {

class FireflyBi : public FireflyBase {
public:
  FireflyBi(const std::string& aId, const std::string& aAlias, const ::Element&);
  virtual ~FireflyBi();

private:
  void retrieveMetricValues() final;

  void retrieveInputMetricValues(Channel& aChannel) final;

  void retrieveOutputMetricValues(Channel& aChannel) final;

  std::unordered_map<const Channel*, SimpleMetric<float>*> mMetricRxPower;
  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricRxLOL, mMetricRxLOS;
  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricTxLOL, mMetricTxLOS, mMetricTxLaserFault;
};

} // namespace swatch
} // namespace serenity

#endif /* __SERENITY_SWATCH_FIREFLYBI_HPP__ */
