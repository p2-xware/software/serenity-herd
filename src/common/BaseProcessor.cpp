
#include "serenity/swatch/BaseProcessor.hpp"


#include <boost/lexical_cast.hpp>

#include <log4cplus/loggingmacros.h>

#include "swatch/action/StateMachine.hpp"

#include "serenity/LocationInfo.hpp"

#include "serenity/swatch/commands/ConfigureClockSynths.hpp"
#include "serenity/swatch/commands/ConfigureCrosspointSwitch.hpp"
#include "serenity/swatch/commands/ConfigureFireflyRx.hpp"
#include "serenity/swatch/commands/ConfigureFireflyTx.hpp"
#include "serenity/swatch/commands/PowerCycle.hpp"
#include "serenity/swatch/commands/PowerOff.hpp"
#include "serenity/swatch/commands/PowerOn.hpp"
#include "serenity/swatch/commands/Program.hpp"
#include "serenity/swatch/commands/ResetFireflys.hpp"
#include "serenity/swatch/utilities.hpp"


namespace serenity {
namespace swatch {

using namespace ::swatch;

const std::string BaseProcessor::CmdIds::kConfigureClockSynths = "configureClockSynths";
const std::string BaseProcessor::CmdIds::kConfigureXpointSwitch = "configureCrosspointSwitch";
const std::string BaseProcessor::CmdIds::kConfigureFireflyRx = "configureFireflyRx";
const std::string BaseProcessor::CmdIds::kConfigureFireflyTx = "configureFireflyTx";
const std::string BaseProcessor::CmdIds::kResetFireflys = "resetFireflys";
const std::string BaseProcessor::CmdIds::kPowerCycle = "powerCycle";
const std::string BaseProcessor::CmdIds::kPowerOff = "powerOff";
const std::string BaseProcessor::CmdIds::kPowerOn = "powerOn";
const std::string BaseProcessor::CmdIds::kProgram = "program";


namespace {

std::string convertToUppercase(const std::string& aString)
{
  std::string lResult(aString);
  std::transform(lResult.cbegin(), lResult.cend(), lResult.begin(), [](unsigned char c) -> char { return std::toupper(c); });
  return lResult;
}

std::string toString(const BoardSeries aSeries)
{
  return (aSeries == BoardSeries::S1 ? "S1" : "Z1");
}

} // namespace (unnamed)


template <BoardSeries Series>
BaseProcessor::BaseProcessor(const swatch::core::AbstractStub& aStub, const IOChannelMap& aIOChannelMap, const size_t aNumberOfPorts, TypeCarrier<Series>) :
  emp::swatch::Processor(
      aStub,
      aNumberOfPorts,
      0,
      [&aIOChannelMap, &aStub](const size_t i) { return extractRxConnection(aIOChannelMap, convertToUppercase(aStub.id), i); },
      [&aIOChannelMap, &aStub](const size_t i) { return extractRxInversion(aIOChannelMap, convertToUppercase(aStub.id), i); },
      [&aIOChannelMap, &aStub](const size_t i) { return extractTxConnection(aIOChannelMap, convertToUppercase(aStub.id), i); },
      [&aIOChannelMap, &aStub](const size_t i) { return extractTxInversion(aIOChannelMap, convertToUppercase(aStub.id), i); }),
  mIOChannelMap(aIOChannelMap),
  mIOChannelMapFpgaId(aStub.id)
{
  // Set logical slot
  const auto lLocationInfo = getBoardLocation();
  if (lLocationInfo.isInCrate()) {
    setLogicalSlot(lLocationInfo.getLogicalSlot());
    setCrateCSPID(lLocationInfo.getShelfCSPId());
  }

  // 1) Register commands
  const auto lBoardSeries = getBoardSeries();
  const auto lBoardType = getBoardType();
  if (lBoardSeries != Series)
    throw std::runtime_error("Serenity BaseProcessor constructor templated for " + toString(Series) + " cannot be used on a " + toString(lBoardSeries) + " board");

  registerCommand<commands::ConfigureClockSynths>(CmdIds::kConfigureClockSynths);
  if (lBoardType == BoardType::Z1_2)
    registerCommand<commands::ConfigureCrosspointSwitch>(CmdIds::kConfigureXpointSwitch);
  registerCommand<commands::ConfigureFireflyRx>(CmdIds::kConfigureFireflyRx);
  registerCommand<commands::ConfigureFireflyTx>(CmdIds::kConfigureFireflyTx);
  registerCommand<commands::ResetFireflys>(CmdIds::kResetFireflys);
  registerCommand<commands::PowerCycle<Series>>(CmdIds::kPowerCycle);
  registerCommand<commands::PowerOff<Series>>(CmdIds::kPowerOff);
  registerCommand<commands::PowerOn<Series>>(CmdIds::kPowerOn);
  registerCommand<commands::Program>(CmdIds::kProgram);

  // 2a) Populate transitions of payload test FSM (EMP commands after power cycle & program)
  /* clang-format off */
  getPayloadTestFSM().setup
      .add(getCommand(CmdIds::kPowerOn))
      .add(getCommand(CmdIds::kProgram));
  emp::swatch::populateTransitions(getPayloadTestFSM());

  // 2b) Populate transitions of link test FSM
  //     setup: EMP commands after power cycle & program
  //     configure: EMP commands after Firefly reset & RX configure, but before TX configure
  //     capture: No Serenity-specific commands
  if (lBoardType == BoardType::Z1_2)
    getLinkTestFSM().setup.add(getCommand(CmdIds::kConfigureXpointSwitch));
  getLinkTestFSM().setup
      .add(getCommand(CmdIds::kPowerOn))
      .add(getCommand(CmdIds::kConfigureClockSynths))
      .add(getCommand(CmdIds::kProgram))
      .add(getCommand(CmdIds::kResetFireflys))
      .add(getCommand(CmdIds::kConfigureFireflyRx))
      .add(getCommand(CmdIds::kConfigureFireflyTx));

  emp::swatch::populateTransitions(getLinkTestFSM());

  // 2c) Populate transitions of slice test FSM
  //     setup: EMP commands after power cycle & program
  //     configure: EMP commands after Firefly reset & RX configure, but before TX configure
  //     capture: No Serenity-specific commands
  if (lBoardType == BoardType::Z1_2)
    getSliceTestFSM().setup.add(getCommand(CmdIds::kConfigureXpointSwitch));
  getSliceTestFSM().setup
      .add(getCommand(CmdIds::kPowerOn))
      .add(getCommand(CmdIds::kConfigureClockSynths))
      .add(getCommand(CmdIds::kProgram))
      .add(getCommand(CmdIds::kResetFireflys))
      .add(getCommand(CmdIds::kConfigureFireflyRx))
      .add(getCommand(CmdIds::kConfigureFireflyTx));
  /* clang-format on */

  emp::swatch::populateTransitions(getSliceTestFSM());
}

// Explicit instantiation of the templated constructor, for both S1 and Z1
template BaseProcessor::BaseProcessor(const swatch::core::AbstractStub&, const IOChannelMap&, const size_t, TypeCarrier<BoardSeries::S1>);
template BaseProcessor::BaseProcessor(const swatch::core::AbstractStub&, const IOChannelMap&, const size_t, TypeCarrier<BoardSeries::Z1>);

BaseProcessor::~BaseProcessor()
{
}


const IOChannelMap& BaseProcessor::getIOChannelMap() const
{
  return mIOChannelMap;
}


bool BaseProcessor::checkPresence() const
{
  // Always returning true since on S1s the FPGA is always present, and on Z1s if a DC is absent then ProcessorAbsent exception would have been thrown in construction
  return true;
}


bool BaseProcessor::extractRxInversion(const IOChannelMap& aIOChannelMap, const std::string& aFpgaId, const size_t i)
{
  // Apply inversion based on I/O channel map on X0 side of inter-site links on Z1 boards. In all other cases, return
  //   false (no inversion), since have arbitrarily chosen to handle net inversion of FPGA-FF links in the FireFly
  if (aIOChannelMap.contains(Direction::Rx, aFpgaId, i)) {
    const auto& lLink = aIOChannelMap.at(Direction::Rx, aFpgaId, i);
    if (lLink.tx.device == (aFpgaId == "X0" ? "X1" : ""))
      return (lLink.getNetPolarity() == Polarity::Invert);
  }

  return false;
}


bool BaseProcessor::extractTxInversion(const IOChannelMap& aIOChannelMap, const std::string& aFpgaId, const size_t i)
{
  // Apply inversion based on I/O channel map on X0 side of inter-site links on Z1 boards. In all other cases, return
  //   false (no inversion), since have arbitrarily chosen to handle net inversion of FPGA-FF links in the FireFly
  if (aIOChannelMap.contains(Direction::Tx, aFpgaId, i)) {
    const auto& lLink = aIOChannelMap.at(Direction::Tx, aFpgaId, i);
    if (lLink.rx.device == (aFpgaId == "X0" ? "X1" : ""))
      return (lLink.getNetPolarity() == Polarity::Invert);
  }

  return false;
}


boost::optional<::swatch::phase2::ChannelID> BaseProcessor::extractRxConnection(const IOChannelMap& aIOChannelMap, const std::string& aFpgaId, const size_t i)
{
  log4cplus::Logger lLogger(log4cplus::Logger::getInstance("serenity.swatch.Processor"));

  if (aIOChannelMap.contains(Direction::Rx, aFpgaId, i)) {
    const auto& lLink = aIOChannelMap.at(Direction::Rx, aFpgaId, i);
    LOG4CPLUS_DEBUG(lLogger, "extractRxConnection(i=" << i << ", device=" << aFpgaId << ") -> FireFly/FPGA " << lLink.tx.device << ", channel " << lLink.tx.channel);
    return ::swatch::phase2::ChannelID { lLink.tx.device, lLink.tx.channel };
  }
  else {
    LOG4CPLUS_DEBUG(lLogger, "extractRxConnection(i=" << i << ", device=" << aFpgaId << ") -> Channel not connected");
    return boost::none;
  }
}


boost::optional<::swatch::phase2::ChannelID> BaseProcessor::extractTxConnection(const IOChannelMap& aIOChannelMap, const std::string& aFpgaId, const size_t i)
{
  log4cplus::Logger lLogger(log4cplus::Logger::getInstance("serenity.swatch.Processor"));

  if (aIOChannelMap.contains(Direction::Tx, aFpgaId, i)) {
    const auto& lLink = aIOChannelMap.at(Direction::Tx, aFpgaId, i);
    LOG4CPLUS_DEBUG(lLogger, "extractTxConnection(i=" << i << ", device=" << aFpgaId << ") -> FireFly/FPGA " << lLink.rx.device << ", channel " << lLink.rx.channel);
    return ::swatch::phase2::ChannelID { lLink.rx.device, lLink.rx.channel };
  }
  else {
    LOG4CPLUS_DEBUG(lLogger, "extractTxConnection(i=" << i << ", device=" << aFpgaId << ") -> Channel not connected");
    return boost::none;
  }
}

} // namespace swatch
} // namespace serenity
