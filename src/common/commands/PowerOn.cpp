
#include "serenity/swatch/commands/PowerOn.hpp"


#include <chrono>

#include <boost/lexical_cast.hpp>

#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"
#include "swatch/phase2/ReadoutInterface.hpp"
#include "swatch/phase2/TTCInterface.hpp"

#include "SMASH/core/Element.hpp"
#include "SMASH/core/Smash.hpp"

#include "serenity/board.hpp"
#include "serenity/power.hpp"
#include "serenity/smash.hpp"
#include "serenity/swatch/Processor.hpp"
#include "serenity/swatch/utilities.hpp"


namespace serenity {
namespace swatch {
namespace commands {

using namespace ::swatch;

template <>
const std::string PowerOn<BoardSeries::S1>::kDescription("Power on FPGA & FireFlys");
template <>
const std::string PowerOn<BoardSeries::Z1>::kDescription("Power on site");


template <BoardSeries Series>
PowerOn<Series>::PowerOn(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, kDescription, aActionable, std::string())
{
}


template <BoardSeries Series>
PowerOn<Series>::~PowerOn()
{
}


template <BoardSeries Series>
action::Command::State PowerOn<Series>::code(const core::ParameterSet& aParams)
{
  setProgress(0., "Starting SMASH session");
  std::unique_ptr<Smash> lSmash(startSmashSession());

  runCoreAction(*lSmash);

  this->getActionable<BaseProcessor>().removeController();
  // ===============================================================================
  // TODO: Update framework to change monitorable settings in more declarative fashion
  //       E.g. additoinal member function, or return struct with map/list of monitoring::Status -> components
  getActionable<BaseProcessor>().getTTC().setMonitoringStatus(core::monitoring::kDisabled);
  getActionable<BaseProcessor>().getReadout().setMonitoringStatus(core::monitoring::kDisabled);
  getActionable<BaseProcessor>().getInputPorts().setMonitoringStatus(core::monitoring::kDisabled);
  getActionable<BaseProcessor>().getOutputPorts().setMonitoringStatus(core::monitoring::kDisabled);
  // ===============================================================================

  return State::kDone;
}


template <>
void PowerOn<BoardSeries::S1>::runCoreAction(Smash& aSmash)
{
  setProgress(0.4, "Enabling power");
  powerOn(aSmash, S1PowerDomain::FPGA);
  powerOn(aSmash, S1PowerDomain::FireFlys);
}


template <>
void PowerOn<BoardSeries::Z1>::runCoreAction(Smash& aSmash)
{
  std::cout << "PowerOn::code - Enabling power" << std::endl;
  std::cout.flush();
  setProgress(0.2, "Enabling power");
  const auto lSite = this->getActionable<Z1Processor>().getSite();
  powerOn(aSmash, convertToZ1PowerDomain(lSite));
  std::this_thread::sleep_for(std::chrono::milliseconds(100));

  setProgress(0.6, "Unprogramming daughter card " + boost::lexical_cast<std::string>(lSite));
  const Z1Device lDevice = lSite == Site::X0 ? Z1Device::X0 : Z1Device::X1;
  Element& lFPGA = *aSmash.GetElement(lSite == Site::X0 ? "DC0:FPGA" : "DC1:FPGA");
  std::cout << "PowerOn::code - Before reload:" << std::endl;
  std::cout.flush();
  {
    std::cout << "    Config state = " << lFPGA.Measure("Config state") << std::endl;
    std::cout.flush();
    const auto lLinks = getSymlinkedZ1Devices();
    std::cout << "    Device symlinks:";
    for (const auto x : lLinks)
      std::cout << " " << x;
    std::cout << std::endl;
    std::cout.flush();
  }
  const auto lReloadTime = std::chrono::steady_clock::now();
  std::cout << "PowerOn::code - Reloading" << std::endl;
  std::cout.flush();
  process(lFPGA, { "Reload" });
  std::cout << "PowerOn::code - Reload done" << std::endl;
  std::cout.flush();
  for (size_t i = 0;; i++) {
    const auto lFileCheckTime = std::chrono::steady_clock::now();
    const auto lLinks = getSymlinkedZ1Devices();

    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    std::cout << "PowerOn::code - sleep " << i << " done. Device symlinks:";
    for (const auto x : lLinks)
      std::cout << " " << x;
    std::cout << std::endl;
    std::cout.flush();

    if (std::count(lLinks.begin(), lLinks.end(), lDevice) == 0) {
      const auto lDuration = std::chrono::duration_cast<std::chrono::microseconds>(lFileCheckTime - lReloadTime);
      setProgress(0.8, "PCIe device file symlinks removed after " + std::to_string(lDuration.count()) + "us");
      break;
    }
    else if (i > 100)
      throw std::runtime_error("PCIe symlinks for " + boost::lexical_cast<std::string>(lSite) + " FPGA are present after unprogramming");
  }
}


template class PowerOn<BoardSeries::S1>;
template class PowerOn<BoardSeries::Z1>;

} // namespace commands
} // namespace swatch
} // namespace serenity
