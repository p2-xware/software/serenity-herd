
#include "serenity/swatch/commands/ConfigureFireflyRx.hpp"


#include <chrono>
#include <thread>

#include "boost/regex.hpp"

#include "SMASH/core/Element.hpp"
#include "SMASH/core/Smash.hpp"

#include "emp/swatch/utilities.hpp"

#include "serenity/board.hpp"
#include "serenity/fireflys.hpp"
#include "serenity/swatch/BaseProcessor.hpp"
#include "serenity/swatch/utilities.hpp"

#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"


namespace serenity {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;
using ::swatch::core::shortVecFmt;


ConfigureFireflyRx::ConfigureFireflyRx(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Configure FireFly RX channels", aActionable, std::string())
{
  registerParameter<std::string>({ "ids"s, "List of corresponding EMP channel indices" }, "");
}


ConfigureFireflyRx::~ConfigureFireflyRx()
{
}


action::Command::State ConfigureFireflyRx::code(const core::ParameterSet& aParams)
{
  auto& lProc = getActionable<BaseProcessor>();

  // TODO: Determine appropriate CDR setting based on optical module type and/or what link FW is instantiated in corresponding FPGA I/O channel
  const CDR lCdrMode = CDR::Low;

  // 1) Construct list of corresponding Firefly modules & channels
  const std::string lIdsString(aParams.get<std::string>("ids"));
  std::unordered_map<std::string, std::vector<uint32_t>> lFireflyChannels;
  std::vector<uint32_t> lSkippedRx;

  setProgress(0., "Starting SMASH session");
  std::unique_ptr<Smash> lSmash(startSmashSession());

  // First add all firefies to lFireflyChannels so that they're all always configured (or disabled)
  const auto lAllFireflySites = getFireflySites(*lSmash);
  for (const auto& x : getFireflys(*lSmash)) {
    if (getFireflyDirection(*lSmash->GetElement(x)) != FireflyDirection::Tx)
      lFireflyChannels[x] = {};
  }

  if (lIdsString == "auto") {
    // ids == auto -> configure everything (in short term)
    // FIXME: In medium/long term, configure based on whether or not firefly modules are disabled, and/or on 'isMasked' property of InputPort
    //        (notably, do this once unused FF channels are auto-disabled by off-board software)
    for (auto& port : lProc.getInputPorts().getPorts()) {
      if ((not port->isPresent()) or port->isMasked() or port->isInLoopback())
        continue;
      if (port->getConnection()) {
        const std::string lDeviceId(port->getConnection()->component);
        if ((lDeviceId == "x0") or (lDeviceId == "x1"))
          continue;
        const auto lFireflyName = convertFireflyIdToSmashName(lDeviceId);
        if (lFireflyChannels.count(lFireflyName) > 0)
          lFireflyChannels.at(lFireflyName).push_back(port->getConnection()->index);
        else if (lAllFireflySites.count(lFireflyName) == 0)
          throw std::runtime_error("Could not find Firefly site named " + lFireflyName + " in SMASH (implies string manipulation error)");
      }
    }
  }
  else { // lIdsString is list of channel index ranges
    const std::vector<uint32_t> lIds(emp::swatch::utilities::parseListOfIndices(lIdsString));
    if (lIds.empty()) {
      setProgress(1.0, "Returning immediately, since channel list is empty");
      return State::kDone;
    }

    // FIXME: Simplify with improved API upstream (in SWATCH - with 'get port by index' function and/or batch 'get connected ChannelID map' method)
    // FIXME: May want to enable/disable monitoring of relevant Firefly channels here (requires SWATCH API update?)
    std::vector<uint32_t> lUnconnectedRx;
    for (auto& port : lProc.getInputPorts().getPorts()) {
      if (std::count(lIds.begin(), lIds.end(), port->getIndex()) > 0) {
        if (not port->isPresent()) {
          lSkippedRx.push_back(port->getIndex());
          continue;
        }
        if (not port->getConnection())
          lUnconnectedRx.push_back(port->getIndex());
        else {
          std::string lFireflyName(port->getConnection()->component);
          lFireflyName = convertFireflyIdToSmashName(lFireflyName);
          if (lFireflyChannels.count(lFireflyName) == 0)
            continue;
          lFireflyChannels.at(lFireflyName).push_back(port->getConnection()->index);
        }
      }
    }
    if (not lUnconnectedRx.empty())
      std::runtime_error("No firefly is connected to RX channels " + shortVecFmt(lUnconnectedRx));
    if (not lSkippedRx.empty())
      setStatusMsg("Rx ports " + shortVecFmt(lSkippedRx) + " not present in FW, and so skipping connected Firefly channels");
  }

  // Sort Firefly channel indices (to simplify messages that list channel indices later on)
  for (auto& x : lFireflyChannels)
    std::sort(x.second.begin(), x.second.end());

  // 2) Configure each Firefly
  size_t i = 1;
  State lExitState = State::kDone;
  for (const auto& x : lFireflyChannels) {
    Element& lElement = *lSmash->GetElement(x.first);
    if (getFireflyDirection(lElement) == FireflyDirection::Tx)
      throw std::runtime_error("Firefly " + x.first + " does not have any RX channels");

    const auto lFirmwareVersion = getFireflyFirmwareVersion(lElement, x.first);
    const std::string lBatch = getFireflyBatch(lFirmwareVersion);
    std::ostringstream lMessage;
    lMessage << "Configuring Firefly " + x.first + " (";
    if (getFireflyDirection(lElement) == FireflyDirection::Bi)
      lMessage << "4+4 bidirectional module";
    else
      lMessage << "12-channel Rx module (" << lBatch << ")";
    if (x.second.empty())
      lMessage << ", disabling all rx channels"
               << ")";
    else
      lMessage << ", rx channels " << shortVecFmt(x.second) << ")";
    setProgress((i / (lFireflyChannels.size() + 1)), lMessage.str());

    for (size_t j = 1;; j++) {
      try {
        const auto lPolarity = [&lProc, &x](size_t i) { return lProc.getIOChannelMap().at(Direction::Tx, convertFireflySmashNameToId(x.first), i).getNetPolarity(); };
        configureFireflyRx(lElement, std::vector<size_t>(x.second.begin(), x.second.end()), lCdrMode, lPolarity);
        break;
      }
      catch (const std::exception& e) {
        if (j == 3)
          throw;
        std::string lRetryMessage("Exception thrown when configuring " + x.first + " (");
        lRetryMessage += core::demangleName(typeid(e).name()) + ", \"" + e.what();
        lRetryMessage += "\"). Retrying (attempt " + std::to_string(j + 1) + ").";
        setStatusMsg(lRetryMessage);
        lExitState = State::kWarning;
      }
    }
    i++;
  }

  setStatusMsg(lFireflyChannels.empty() ? "No Firefly modules configured" : "Firefly modules configured successfully");
  return (lSkippedRx.empty() ? lExitState : State::kWarning);
}

} // namespace commands
} // namespace swatch
} // namespace serenity
