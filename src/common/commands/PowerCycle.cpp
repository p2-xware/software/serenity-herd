
#include "serenity/swatch/commands/PowerCycle.hpp"


#include "SMASH/core/Smash.hpp"

#include "serenity/board.hpp"
#include "serenity/power.hpp"
#include "serenity/swatch/Processor.hpp"
#include "serenity/swatch/utilities.hpp"


namespace serenity {
namespace swatch {
namespace commands {

using namespace ::swatch;

template <>
const std::string PowerCycle<BoardSeries::S1>::kDescription("Power cycle FPGA & FireFlys");
template <>
const std::string PowerCycle<BoardSeries::Z1>::kDescription("Power cycle site");


template <BoardSeries Series>
PowerCycle<Series>::PowerCycle(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, kDescription, aActionable, std::string())
{
}


template <BoardSeries Series>
PowerCycle<Series>::~PowerCycle()
{
}


template <>
action::Command::State PowerCycle<BoardSeries::S1>::code(const core::ParameterSet& aParams)
{
  setProgress(0., "Starting SMASH session");
  std::unique_ptr<Smash> lSmash(startSmashSession());

  setProgress(0.2, "Disabling power");
  this->getActionable<BaseProcessor>().removeController();
  powerOff(*lSmash, S1PowerDomain::FireFlys);
  powerOff(*lSmash, S1PowerDomain::FPGA);

  setProgress(0.6, "Enabling power");
  powerOn(*lSmash, S1PowerDomain::FPGA);
  powerOn(*lSmash, S1PowerDomain::FireFlys);

  return State::kDone;
}


template <>
action::Command::State PowerCycle<BoardSeries::Z1>::code(const core::ParameterSet& aParams)
{
  setProgress(0., "Starting SMASH session");
  std::unique_ptr<Smash> lSmash(startSmashSession());

  setProgress(0.2, "Disabling power");
  this->getActionable<Z1Processor>().removeController();
  const auto lSite = this->getActionable<Z1Processor>().getSite();
  powerOff(*lSmash, convertToZ1PowerDomain(lSite));

  setProgress(0.6, "Enabling power");
  powerOn(*lSmash, convertToZ1PowerDomain(lSite));

  return State::kDone;
}


template class PowerCycle<BoardSeries::S1>;
template class PowerCycle<BoardSeries::Z1>;

} // namespace commands
} // namespace swatch
} // namespace serenity
