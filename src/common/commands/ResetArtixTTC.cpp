#include "serenity/swatch/commands/ResetArtixTTC.hpp"


#include <thread>

#include "serenity/ArtixTTCController.hpp"

#include "serenity/swatch/Artix.hpp"

#include "swatch/core/rules/IsAmong.hpp"


namespace serenity {
namespace swatch {
namespace commands {

using namespace ::swatch;

ResetArtixTTC::ResetArtixTTC(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Reset", "Reset and configure TTC interface/generator", aActionable, std::string())
{
  registerParameter<std::string>("source", "external", core::rules::IsAmong<std::string>({ "external", "internal" }));
}


ResetArtixTTC::~ResetArtixTTC()
{
}


action::Command::State ResetArtixTTC::code(const core::ParameterSet& aParams)
{
  ArtixTTCController& lController = getActionable<Artix>().getController();

  const std::string& lClockSource = aParams.get<std::string>("source");
  setProgress(0., "Resetting clocks (source=" + lClockSource + ")");

  bool lClockLocked = lController.reset(lClockSource == "external", lClockSource == "external");
  if (not lClockLocked) {
    setStatusMsg("Clock not locked to external source");
    return State::kError;
  }

  setProgress(0.5, "TTC clock/data source reset. Reading status registers ...");
  std::this_thread::sleep_for(std::chrono::seconds(1));

  TTCStatus lStatus = lController.readStatus();
  if (not serenity::checkStatus(lStatus)) {
    setStatusMsg("Status registers indicate a problem!");
    return State::kError;
  }

  setResult(std::string("Reset completed"));
  return State::kDone;
}

} // namespace commands
} // namespace swatch
} // namespace serenity
