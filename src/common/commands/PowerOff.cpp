
#include "serenity/swatch/commands/PowerOff.hpp"


#include "SMASH/core/Smash.hpp"

#include "serenity/board.hpp"
#include "serenity/power.hpp"
#include "serenity/swatch/Processor.hpp"
#include "serenity/swatch/utilities.hpp"

#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"
#include "swatch/phase2/ReadoutInterface.hpp"
#include "swatch/phase2/TTCInterface.hpp"


namespace serenity {
namespace swatch {
namespace commands {

using namespace ::swatch;

template <>
const std::string PowerOff<BoardSeries::S1>::kDescription("Power off FPGA & FireFlys");
template <>
const std::string PowerOff<BoardSeries::Z1>::kDescription("Power off site");


template <BoardSeries Series>
PowerOff<Series>::PowerOff(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, kDescription, aActionable, std::string())
{
}


template <BoardSeries Series>
PowerOff<Series>::~PowerOff()
{
}


template <BoardSeries Series>
action::Command::State PowerOff<Series>::code(const core::ParameterSet& aParams)
{
  setProgress(0., "Starting SMASH session");
  std::unique_ptr<Smash> lSmash(startSmashSession());

  setProgress(0.2, "Disabling power");
  this->getActionable<BaseProcessor>().removeController();
  runCoreAction(*lSmash);

  // ===============================================================================
  // TODO: Update framework to change monitorable settings in more declarative fashion
  //       E.g. additoinal member function, or return struct with map/list of monitoring::Status -> components
  getActionable<BaseProcessor>().getTTC().setMonitoringStatus(core::monitoring::kDisabled);
  getActionable<BaseProcessor>().getReadout().setMonitoringStatus(core::monitoring::kDisabled);
  getActionable<BaseProcessor>().getInputPorts().setMonitoringStatus(core::monitoring::kDisabled);
  getActionable<BaseProcessor>().getOutputPorts().setMonitoringStatus(core::monitoring::kDisabled);
  // ===============================================================================

  return State::kDone;
}


template <>
void PowerOff<BoardSeries::S1>::runCoreAction(Smash& aSmash)
{
  powerOff(aSmash, S1PowerDomain::FireFlys);
  powerOff(aSmash, S1PowerDomain::FPGA);
}


template <>
void PowerOff<BoardSeries::Z1>::runCoreAction(Smash& aSmash)
{
  const auto lSite = this->getActionable<Z1Processor>().getSite();
  powerOff(aSmash, convertToZ1PowerDomain(lSite));
}


template class PowerOff<BoardSeries::S1>;
template class PowerOff<BoardSeries::Z1>;

} // namespace commands
} // namespace swatch
} // namespace serenity
