
#include "serenity/swatch/commands/ConfigureCrosspointSwitch.hpp"


#include "SMASH/core/Smash.hpp"

#include "boost/lexical_cast.hpp"

#include "serenity/board.hpp"
#include "serenity/swatch/Processor.hpp"
#include "serenity/swatch/utilities.hpp"


namespace serenity {
namespace swatch {
namespace commands {

using namespace ::swatch;

ConfigureCrosspointSwitch::ConfigureCrosspointSwitch(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Configure crosspoint switch", aActionable, std::string())
{
}


ConfigureCrosspointSwitch::~ConfigureCrosspointSwitch()
{
}


action::Command::State ConfigureCrosspointSwitch::code(const core::ParameterSet& aParams)
{
  setProgress(0., "Starting SMASH session");
  std::unique_ptr<Smash> lSmash(startSmashSession());

  const auto lSite = this->getActionable<Z1Processor>().getSite();
  setProgress(0.5, "Routing TCDS2 pins to/from " + boost::lexical_cast<std::string>(lSite));
  configureCrosspointSwitch(*lSmash, lSite);

  setProgress(1.0, "Crosspoint switch configured");
  return State::kDone;
}


} // namespace commands
} // namespace swatch
} // namespace serenity
