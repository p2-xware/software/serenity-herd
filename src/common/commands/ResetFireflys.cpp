
#include "serenity/swatch/commands/ResetFireflys.hpp"


#include <chrono>
#include <thread>

#include "boost/regex.hpp"

#include "SMASH/core/Element.hpp"
#include "SMASH/core/Smash.hpp"

#include "emp/swatch/utilities.hpp"

#include "serenity/board.hpp"
#include "serenity/fireflys.hpp"
#include "serenity/swatch/BaseProcessor.hpp"
#include "serenity/swatch/utilities.hpp"

#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"


namespace serenity {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;
using ::swatch::core::shortVecFmt;


ResetFireflys::ResetFireflys(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Reset FireFly modules", aActionable, std::string())
{
}


ResetFireflys::~ResetFireflys()
{
}


action::Command::State ResetFireflys::code(const core::ParameterSet& aParams)
{
  setProgress(0., "Starting SMASH session");
  std::unique_ptr<Smash> lSmash(startSmashSession());

  // 1) Construct list of all connected Fireflys
  const auto lAllFireflySites = getFireflySites(*lSmash);
  std::set<std::string> lFireflyNames;

  for (auto& port : getActionable<BaseProcessor>().getInputPorts().getPorts()) {
    if (not port->getConnection())
      continue;

    const auto lFireflyName = convertFireflyIdToSmashName(port->getConnection()->component);
    if (lSmash->ElementList().count(lFireflyName) > 0)
      lFireflyNames.insert(lFireflyName);
    else if (lAllFireflySites.count(lFireflyName) == 0)
      throw std::runtime_error("Could not find Firefly site named " + lFireflyName + " in SMASH (implies string manipulation error)");
  }

  for (auto& port : getActionable<BaseProcessor>().getOutputPorts().getPorts()) {
    if (not port->getConnection())
      continue;

    const auto lFireflyName = convertFireflyIdToSmashName(port->getConnection()->component);
    if (lSmash->ElementList().count(lFireflyName) > 0)
      lFireflyNames.insert(lFireflyName);
  }

  // 2) Reset the modules
  size_t i = 1;
  for (const auto lName : lFireflyNames) {
    Element& lElement = *lSmash->GetElement(lName);
    const auto lType = lElement.Attribute("device direction");
    const auto lFirmwareVersion = getFireflyFirmwareVersion(lElement, lName);
    const std::string lBatch = getFireflyBatch(lFirmwareVersion);
    std::ostringstream lMessage;
    if (lType == "Bidirectional")
      lMessage << " (4+4 bidirectional module)";
    else
      lMessage << " (12-channel " << lType << " module (" << lBatch << "))";
    setProgress(i / (lFireflyNames.size() + 1), "Resetting Firefly " + lName + lMessage.str());
    resetFirefly(lElement);
    i++;
  }

  // 3) Sleep after resetting the modules
  std::this_thread::sleep_for(std::chrono::seconds(2));

  setProgress(1., "Firefly modules reset successfully");
  return State::kDone;
}

} // namespace commands
} // namespace swatch
} // namespace serenity
