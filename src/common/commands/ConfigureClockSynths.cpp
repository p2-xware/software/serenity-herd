
#include "serenity/swatch/commands/ConfigureClockSynths.hpp"


#include "SMASH/core/Smash.hpp"

#include "serenity/board.hpp"
#include "serenity/clocks.hpp"
#include "serenity/swatch/Processor.hpp"
#include "serenity/swatch/utilities.hpp"

#include "swatch/core/utilities.hpp"


namespace serenity {
namespace swatch {
namespace commands {

using namespace ::swatch;

ConfigureClockSynths::ConfigureClockSynths(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Configure clock synths", aActionable, std::string())
{
}


ConfigureClockSynths::~ConfigureClockSynths()
{
}


action::Command::State ConfigureClockSynths::code(const core::ParameterSet& aParams)
{
  setProgress(0., "Starting SMASH session");
  std::unique_ptr<Smash> lSmash(startSmashSession());

  setProgress(0.2, "Configuring clock synths");
  State lExitState = State::kDone;
  for (size_t i = 1;; i++) {
    try {
      Site lSite = Site::X0;
      if (getBoardSeries() == BoardSeries::Z1)
        lSite = this->getActionable<Z1Processor>().getSite();
      configureClockSynths(*lSmash, getBoardType(), lSite, {});
      break;
    }
    catch (const std::exception& e) {
      if (i == 3)
        throw;
      std::string lMessage("Exception thrown when configuring clock (");
      lMessage += core::demangleName(typeid(e).name()) + ", \"" + e.what();
      lMessage += "\"). Retrying (attempt " + std::to_string(i + 1) + ").";
      setStatusMsg(lMessage);
      lExitState = State::kWarning;
    }
  }

  setProgress(1.0, "Clock synths configured");
  return lExitState;
}


} // namespace commands
} // namespace swatch
} // namespace serenity
