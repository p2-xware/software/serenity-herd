
#include "serenity/swatch/commands/Program.hpp"


#include <chrono>
#include <string>
#include <thread>

#include <boost/lexical_cast.hpp>

#include "SMASH/core/Smash.hpp"

#include "serenity/board.hpp"
#include "serenity/jtag.hpp"
#include "serenity/swatch/Processor.hpp"
#include "serenity/swatch/utilities.hpp"


namespace serenity {
namespace swatch {
namespace commands {

using namespace ::swatch;

Program::Program(const std::string& aId, action::ActionableObject& aActionable) :
  AbstractProgram(aId, aActionable, ".bit")
{
}


Program::~Program()
{
}


void Program::program(const std::string& aProgrammingFilePath)
{
  setProgress(0.2, "Starting SMASH session");
  std::unique_ptr<Smash> lSmash(startSmashSession());

  boost::optional<Site> lSite;
  if (getBoardSeries() == BoardSeries::Z1)
    lSite = this->getActionable<Z1Processor>().getSite();
  setProgress(0.4, "Programming FPGA");
  programFPGA(*lSmash, lSite, aProgrammingFilePath, 111.0);

  // FIXME: Pragmatically added 200ms sleep to avoid COMX crashes on Serenitys in 904
  //        with latest COMX module - should find another solution (e.g. somehow polling
  //        status/readiness of PCIe connection), or push this upstream
  std::this_thread::sleep_for(std::chrono::milliseconds(200));
}


} // namespace commands
} // namespace swatch
} // namespace serenity
