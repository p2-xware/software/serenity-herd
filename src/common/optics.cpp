


#include <algorithm>
#include <iostream>
#include <list>
#include <string>

#include "SMASH/core/Element.hpp"
#include "SMASH/core/Smash.hpp"

#include "swatch/phase2/Board.hpp"
#include "swatch/phase2/EmptySocket.hpp"

#include "serenity/board.hpp"
#include "serenity/fireflys.hpp"
#include "serenity/swatch/FireflyBi.hpp"
#include "serenity/swatch/FireflyRx16G.hpp"
#include "serenity/swatch/FireflyRx25G.hpp"
#include "serenity/swatch/FireflyTx16G.hpp"
#include "serenity/swatch/FireflyTx25G.hpp"
#include "serenity/swatch/utilities.hpp"


namespace serenity {
namespace swatch {

using ::swatch::phase2::Board;
using ::swatch::phase2::OpticalModule;


namespace {

const std::vector<std::string> kFrontPanelConnectorsS1 { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11" };
const std::vector<std::string> kFrontPanelConnectorsZ1 { "T1", "T2", "T3", "T4", "T5", "T6", "B1", "B2", "B3", "B4", "B5", "B6" };

} // namespace (unnamed)


OpticalModule* createFireflyInstance(const std::string& aId, const std::string& aAlias, const ::Element& aFirefly)
{
  switch (getFireflyType(aFirefly)) {
    case FireflyType::Bi25G:
      return new FireflyBi(aId, aAlias, aFirefly);
    case FireflyType::RxCERNB:
    case FireflyType::Rx16G:
      return new FireflyRx16G(aId, aAlias, aFirefly);
    case FireflyType::Rx25G:
      return new FireflyRx25G(aId, aAlias, aFirefly);
    case FireflyType::TxCERNB:
    case FireflyType::Tx16G:
      return new FireflyTx16G(aId, aAlias, aFirefly);
    case FireflyType::Tx25G:
      return new FireflyTx25G(aId, aAlias, aFirefly);
  }

  throw std::runtime_error("Firefly '" + aId + "' is of unsupported type.");
}


OpticalModule* createEmptySocketInstance(const std::string& aId, const std::string& aAlias)
{
  using ::swatch::phase2::EmptySocket;

  switch (getBoardType()) {
    case BoardType::Z1_1: {
      const size_t lIndex = std::stoul(aId.substr(3));
      // 4+4 Firefly @ Jn:6, :7, :9, :11, :13 and :15
      if (lIndex <= 15)
        return new EmptySocket(aId, aAlias, { 1, 2, 3, 4 }, { 1, 2, 3, 4 });
      // 12-channel TX @ Jn:19, :23, :27, :31, :35
      else if (((lIndex - 17) % 4) == 2)
        return new EmptySocket(aId, aAlias, 0, 12);
      // 12-channel RX @ Jn:17, :21, :25, :29, :33
      else if (((lIndex - 17) % 4) == 0)
        return new EmptySocket(aId, aAlias, 12, 0);
      else
        throw std::runtime_error("createEmptySocketInstance: Did not recognise Firefly site " + aId + " (" + aAlias + ")");
    }

    case BoardType::Z1_2: {
      const Site lSite = aId.find("FF0_") == 0 ? Site::X0 : Site::X1;
      const size_t lIndex = std::stoul(aId.substr(4));

      const auto lDCType = getDaughterCardType(lSite);
      if (lDCType == boost::none) {
        if ((lIndex == 0) or (lIndex == 17)) // 4+4 Firefly
          return new EmptySocket(aId, aAlias, { 1, 2, 3, 4 }, { 1, 2, 3, 4 });
        else // Default to 12-channel RX Firefly (rather than TX - arbitrary choice)
          return new EmptySocket(aId, aAlias, 12, 0);
      }

      switch (*lDCType) {
        case DaughterCardType::KU15P_SO2:
        case DaughterCardType::VU9P_SO2:
        case DaughterCardType::VU7P_SO1_v1_1:
        case DaughterCardType::VU13P_SO2:
          if ((lIndex == 0) or (lIndex == 17)) // 4+4 Firefly
            return new EmptySocket(aId, aAlias, { 1, 2, 3, 4 }, { 1, 2, 3, 4 });
          else if ((lIndex % 2) == 1) // 12-channel TX Firefly
            return new EmptySocket(aId, aAlias, 0, 12);
          else // 12-channel RX Firefly
            return new EmptySocket(aId, aAlias, 12, 0);
        default:
          throw std::logic_error("createEmptySocketInstance: Reached default of DC switch statement with socket '" + aId + "' ('" + aAlias + "')");
      }
    }

    default: // S1.x
      if (aId.find("rx") != std::string::npos)
        return new EmptySocket(aId, aAlias, 12, 0);
      else if (aId.find("tx") != std::string::npos)
        return new EmptySocket(aId, aAlias, 0, 12);
      else
        return new EmptySocket(aId, aAlias, { 1, 2, 3, 4 }, { 1, 2, 3, 4 });
  }

  throw std::logic_error("createEmptySocketInstance: Unexpectedly reached end of function for socket '" + aId + "' ('" + aAlias + "')");
}


std::pair<std::vector<OpticalModule*>, std::vector<std::string>> createOpticalModules()
{
  std::unique_ptr<Smash> lSmash(startSmashSession());

  const auto lFireflySites = getFireflySites(*lSmash);
  const auto lOccupiedFireflySites = getOccupiedFireflySites(*lSmash);

  std::vector<OpticalModule*> lModules;
  std::cout << std::setfill(' ');
  for (const auto& lSmashName : lFireflySites) {
    std::string lAlias(convertFireflySmashNameToId(lSmashName));
    std::string lId(lAlias);

    if ((lSmash->ElementList().count(lSmashName) > 0) and lOccupiedFireflySites.count(lSmashName)) {
      std::cout << "  Firefly " << std::setw(6) << lId << " @ " << std::setw(6) << lSmashName << " -> Present" << std::endl;
      lModules.push_back(createFireflyInstance(lId, lAlias, *lSmash->GetElement(lSmashName)));
    }
    else {
      std::cout << "  Firefly " << std::setw(6) << lId << " @ " << std::setw(6) << lSmashName << " -> Absent" << std::endl;
      lModules.push_back(createEmptySocketInstance(lId, lAlias));
    }
  }

  return { lModules, getBoardSeries() == BoardSeries::S1 ? kFrontPanelConnectorsS1 : kFrontPanelConnectorsZ1 };
}


SWATCH_REGISTER_PHASE2_OPTICS_CREATOR("serenity", serenity::swatch::createOpticalModules);


} // namespace swatch
} // namespace serenity
