
#include "serenity/swatch/Zynq.hpp"


#include "swatch/core/Factory.hpp"

#include "serenity/LocationInfo.hpp"
#include "serenity/swatch/utilities.hpp"


SWATCH_REGISTER_CLASS(serenity::swatch::Zynq)


namespace serenity {
namespace swatch {

using namespace ::swatch;


Zynq::Zynq(const core::AbstractStub& aStub) :
  ServiceModule(aStub)
{
}


Zynq::~Zynq()
{
}


void Zynq::retrieveMetricValues()
{
  const auto lLocationInfo = getBoardLocation();
  setMetric(mMetricLogicalSlot, uint16_t(lLocationInfo.getLogicalSlot()));
  setMetric(mMetricShelfAddress, lLocationInfo.getShelfAddress());
}


} // namespace swatch
} // namespace serenity
