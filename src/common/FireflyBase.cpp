
#include "serenity/swatch/FireflyBase.hpp"

#include <regex>

#include <boost/algorithm/string.hpp>

#include "SMASH/core/Element.hpp"
#include "SMASH/core/Smash.hpp"

#include "serenity/board.hpp"
#include "serenity/swatch/utilities.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace serenity {
namespace swatch {

using namespace ::swatch;

// FIXME: For optimal performance: Add callback for 'finishedUpdatingMetrics' - so that can keep SMASH session
//        alive across retrieveMetricValues methods of different object instances, without blocking creation of
//        MetricUpdateGuard, but ensuring that SMASH session is released when MetricUpdateGuard is destroyed

FireflyBase::FireflyBase(const std::string& aId, const std::string& aAlias, const ::Element& aFirefly, const std::vector<size_t>& aRxIndices, const std::vector<size_t>& aTxIndices) :
  OpticalModule(aId, aAlias, aRxIndices, aTxIndices),
  mPartStr(aFirefly.Attribute("device name")),
  mPart(registerProperty<std::string>("Part")),
  mSerialNumber(registerProperty<std::string>("Serial number")),
  mFirmwareVersion(registerProperty<std::string>("Firmware version")),
  mElementName(aFirefly.Attribute("name")),
  mMetricTemperature(registerMetric<float>("temperature")),
  mMetricPeakTemperature(registerMetric<float>("peakTemperature"))
{
  mPart.setDescription("Firefly part code");
  mMetricPeakTemperature.setUnit("C");
  mMetricPeakTemperature.setFormat(core::format::kFixedPoint, 1);
  mMetricTemperature.setUnit("C");
  mMetricTemperature.setFormat(core::format::kFixedPoint, 1);

  setWarningCondition(mMetricTemperature, ::swatch::core::GreaterThanCondition<float>(50));
}


FireflyBase::~FireflyBase()
{
}


bool FireflyBase::checkPresence() const
{
  return true;
}


void FireflyBase::retrievePropertyValues()
{
  set(mPart, mPartStr);

  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));
  set(mSerialNumber, lFirefly.Measure("Serial Number"));
  set(mFirmwareVersion, lFirefly.Measure("Firmware Version"));
}


std::pair<float, std::string> FireflyBase::measurePhysicalQuantity(Element& aElement, const std::string& aName)
{
  const std::string lValueString(aElement.Measure(aName));

  const std::regex lRegex(R"(([\+-]?[0-9]+(\.[0-9]+)?)([a-zA-Z]*))"); /* outermost brackets are part of raw string syntax */
  std::smatch lMatchResults;

  if (std::regex_match(lValueString, lMatchResults, lRegex)) {
    return { std::stof(lMatchResults[0]), lMatchResults[lMatchResults.size() - 1] };
  }
  else
    throw std::runtime_error("Measurement did not match regex for physical quantity ('" + aName + "' has value '" + lValueString + "')");
}


std::map<size_t, bool> FireflyBase::measureChannelFlag(Element& aElement, const std::string& aName, const std::vector<size_t> aChannels, const std::unordered_map<std::string, bool>& aInterpretationMap)
{
  std::string lValueString(aElement.Measure(aName));
  boost::algorithm::trim(lValueString);

  std::vector<std::string> lEntries;
  boost::split(lEntries, lValueString, boost::is_any_of(";"));
  if (lEntries.at(lEntries.size() - 1) == "")
    lEntries.pop_back();

  const std::regex lRegex(R"(\s*([A-Za-z0-9\s].*[A-Za-z0-9])\s*:\s*([0-9]+\s*(?:,\s*[0-9]+\s*)*))"); /* outermost brackets are part of raw string syntax */
  std::map<size_t, bool> lResult;
  for (auto& x : lEntries) {
    std::smatch lMatchResults;
    if (std::regex_match(x, lMatchResults, lRegex)) {
      std::string lIndexList(lMatchResults[2]);
      std::vector<std::string> lIndices;
      boost::split(lIndices, lIndexList, boost::is_any_of(", "), boost::token_compress_on);

      if (aInterpretationMap.count(lMatchResults[1]) == 0)
        throw std::runtime_error("Measurement '" + aName + "' (value '" + lValueString + "'): Cannot find entry for '" + lMatchResults[1].str() + "' in interpretation map");

      for (const auto x : lIndices)
        lResult[std::stoul(x)] = aInterpretationMap.at(lMatchResults[1]);
    }
    else
      throw std::runtime_error("Measurement '" + aName + "' (value '" + lValueString + "'): Entry '" + x + "' did not match regex");
  }

  for (const size_t i : aChannels) {
    if (lResult.count(i) == 0)
      throw std::runtime_error("Measurement '" + aName + "' (value '" + lValueString + "'): Value for channel " + std::to_string(i) + " is missing");
  }

  return lResult;
}


std::map<size_t, std::string> FireflyBase::measureChannelSetting(Element& aElement, const std::string& aName, const std::vector<size_t> aChannels)
{
  std::string lValueString(aElement.Measure(aName));
  boost::algorithm::trim(lValueString);

  std::vector<std::string> lEntries;
  boost::split(lEntries, lValueString, boost::is_any_of(";"));
  if (lEntries.at(lEntries.size() - 1) == "")
    lEntries.pop_back();

  const std::regex lRegex(R"(\s*([A-Za-z0-9\s].*[A-Za-z0-9])\s*:\s*([0-9]+\s*(?:,\s*[0-9]+\s*)*))"); /* outermost brackets are part of raw string syntax */
  std::map<size_t, std::string> lResult;
  for (auto& x : lEntries) {
    std::smatch lMatchResults;
    if (std::regex_match(x, lMatchResults, lRegex)) {
      std::string lIndexList(lMatchResults[2]);
      std::vector<std::string> lIndices;
      boost::split(lIndices, lIndexList, boost::is_any_of(", "), boost::token_compress_on);

      for (const auto x : lIndices)
        lResult[std::stoul(x)] = lMatchResults[1];
    }
    else
      throw std::runtime_error("Measurement '" + aName + "' (value '" + lValueString + "'): Entry '" + x + "' did not match regex");
  }

  for (const size_t i : aChannels) {
    if (lResult.count(i) == 0)
      throw std::runtime_error("Measurement '" + aName + "' (value '" + lValueString + "'): Value for channel " + std::to_string(i) + " is missing");
  }

  return lResult;
}

const std::unordered_map<std::string, bool> FireflyBase::kAlarmValueMap = {
  { "No fault", false },
  { "Fault", true }
};

const std::unordered_map<std::string, bool> FireflyBase::kEnableValueMap = {
  { "Disable", false },
  { "Enable", true }
};

const std::unordered_map<std::string, bool> FireflyBase::kPolarityInvertedValueMap = {
  { "Normal", false },
  { "Invert", true }
};


} // namespace swatch
} // namespace serenity