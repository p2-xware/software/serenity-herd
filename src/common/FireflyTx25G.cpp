#include "serenity/swatch/FireflyTx25G.hpp"

#include "SMASH/core/Element.hpp"
#include "SMASH/core/Smash.hpp"

#include "serenity/board.hpp"
#include "serenity/fireflys.hpp"
#include "serenity/swatch/utilities.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace serenity {
namespace swatch {

const std::vector<size_t> FireflyTx25G::kChannels { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

FireflyTx25G::FireflyTx25G(const std::string& aId, const std::string& aAlias, const ::Element& aFirefly) :
  FireflyBase(aId, aAlias, aFirefly, {}, kChannels)
{
  if ((getBoardSeries() == BoardSeries::S1) and (getBoardType() != BoardType::S1_1)) {
    mMetricVoltage = &registerMetric<float>("voltage", "Voltage");
    mMetricVoltage->setUnit("V");
    mMetricVoltage->setFormat(::swatch::core::format::kFixedPoint, 1);
    // N.B. As of SMASH commit 049035af (merge request 179), FF voltage no longer reverts
    //      to 3.3V if you access FF I2C before setting Tx voltage in a SMASH session
    setWarningCondition(*mMetricVoltage, ::swatch::core::LessThanCondition<float>(3.75));
  }

  for (auto* channel : getOutputs()) {
    // Properties
    mPropertyState[channel] = &channel->registerProperty<std::string>("State");
    mPropertyPolarity[channel] = &channel->registerProperty<std::string>("Polarity");
    mPropertyEqualization[channel] = &channel->registerProperty<std::string>("Equalization");
    mPropertySquelch[channel] = &channel->registerProperty<std::string>("Squelch");
    mPropertyCDR[channel] = &channel->registerProperty<std::string>("CDR");

    // Metrics
    mMetricLOL[channel] = &channel->registerMetric<bool>("LOL", "CDR lock lost (LOL)", ::swatch::core::EqualCondition<bool>(true));
    mMetricLaserFault[channel] = &channel->registerMetric<bool>("laserFault", "Laser fault", ::swatch::core::EqualCondition<bool>(true));

    // Declare that this monitorable object will update each of the above per-channel metrics
    channel->delegateMetricUpdate(mMetricLOL.at(channel)->getId(), *this);
    channel->delegateMetricUpdate(mMetricLaserFault.at(channel)->getId(), *this);
  }
}


FireflyTx25G::~FireflyTx25G()
{
}


void FireflyTx25G::retrieveMetricValues()
{
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  setMetric(mMetricTemperature, measurePhysicalQuantity(lFirefly, "Temperature").first);
  setMetric(mMetricPeakTemperature, measurePhysicalQuantity(lFirefly, "Peak Temperature").first);

  if (mMetricVoltage) {
    const auto lVoltage = readFireflyVoltage(*lSmash, mElementName);
    setMetric(*mMetricVoltage, float(lVoltage == kFirefly3V3 ? 3.3 : 3.75));
  }

  // Don't continue if all channels are disabled
  if (std::all_of(getOutputs().cbegin(), getOutputs().cend(),
                  [](auto channel) { return channel->getMonitoringStatus() == ::swatch::core::monitoring::kDisabled; })) {
    return;
  }
  const auto lLOL = measureChannelFlag(lFirefly, "CDR LOL alarms", kChannels, kAlarmValueMap);
  const auto lLaserFault = measureChannelFlag(lFirefly, "Laser fault alarms", kChannels, kAlarmValueMap);

  for (auto* channel : getOutputs()) {
    if (channel->getMonitoringStatus() == ::swatch::core::monitoring::kDisabled) {
      continue;
    }
    setMetric(*mMetricLOL.at(channel), lLOL.at(channel->getIndex()));
    setMetric(*mMetricLaserFault.at(channel), lLaserFault.at(channel->getIndex()));
  }
}


void FireflyTx25G::retrieveOutputPropertyValues(Channel& aChannel)
{
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  const auto lEqualization = measureChannelSetting(lFirefly, "Equalization", kChannels);
  const auto lInvPolarity = measureChannelFlag(lFirefly, "Polarity", kChannels, kPolarityInvertedValueMap);
  const auto lSquelch = measureChannelFlag(lFirefly, "Squelch", kChannels, kEnableValueMap);
  const auto lCDR = measureChannelFlag(lFirefly, "CDR", kChannels, kEnableValueMap);
  const auto lEnabled1 = measureChannelFlag(lFirefly, "Channel", kChannels, kEnableValueMap);
  const auto lEnabled2 = measureChannelFlag(lFirefly, "Output", kChannels, kEnableValueMap);

  set<std::string>(*mPropertyEqualization.at(&aChannel), lEqualization.at(aChannel.getIndex()));
  set<std::string>(*mPropertyPolarity.at(&aChannel), std::string(lInvPolarity.at(aChannel.getIndex()) ? "Inverted" : "Normal"));
  set<std::string>(*mPropertySquelch.at(&aChannel), lSquelch.at(aChannel.getIndex()) ? "Enabled" : "Disabled");
  set<std::string>(*mPropertyCDR.at(&aChannel), lCDR.at(aChannel.getIndex()) ? "Enabled" : "Disabled");

  // Set state
  if (lEnabled1.at(aChannel.getIndex()) and lEnabled2.at(aChannel.getIndex()))
    set<std::string>(*mPropertyState.at(&aChannel), "Enabled");
  else if (lEnabled1.at(aChannel.getIndex()) or lEnabled2.at(aChannel.getIndex()))
    set<std::string>(*mPropertyState.at(&aChannel), "Mixed");
  else
    set<std::string>(*mPropertyState.at(&aChannel), "Disabled");
}


void FireflyTx25G::retrieveOutputMetricValues(Channel& aChannel)
{
  // Monitoring data for output channels updated by parent instead (since called delegateUpdateMetrics in CTOR)
}

} // namespace swatch
} // namespace serenity