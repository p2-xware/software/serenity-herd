#include "serenity/swatch/FireflyRx16G.hpp"

#include "SMASH/core/Element.hpp"
#include "SMASH/core/Smash.hpp"

#include "serenity/board.hpp"
#include "serenity/swatch/utilities.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace serenity {
namespace swatch {

using namespace ::swatch;

const std::vector<size_t> FireflyRx16G::kChannels { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

// Firefly Rx class for both 16G and CERN-B parts

FireflyRx16G::FireflyRx16G(const std::string& aId, const std::string& aAlias, const ::Element& aFirefly) :
  FireflyBase(aId, aAlias, aFirefly, kChannels, {})
{
  for (auto* channel : getInputs()) {
    // Properties
    mPropertyState[channel] = &channel->registerProperty<std::string>("State");
    mPropertyAmplitude[channel] = &channel->registerProperty<std::string>("Amplitude");
    mPropertyPolarity[channel] = &channel->registerProperty<std::string>("Polarity");

    // Metrics
    mMetricLOS[channel] = &channel->registerMetric<bool>("LOS", "Signal lost (LOS)", ::swatch::core::EqualCondition<bool>(true));

    // Declare that this monitorable object will update each of the above per-channel metrics
    channel->delegateMetricUpdate(mMetricLOS.at(channel)->getId(), *this);
  }
}


FireflyRx16G::~FireflyRx16G()
{
}


void FireflyRx16G::retrieveInputPropertyValues(Channel& aChannel)
{
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  const auto lAmplitude = measureChannelSetting(lFirefly, "Amplitude", kChannels);
  const auto lInvPolarity = measureChannelFlag(lFirefly, "Polarity", kChannels, kPolarityInvertedValueMap);
  const auto lEnabled = measureChannelFlag(lFirefly, "Channel", kChannels, kEnableValueMap);

  set<std::string>(*mPropertyAmplitude.at(&aChannel), lAmplitude.at(aChannel.getIndex()));
  set<std::string>(*mPropertyPolarity.at(&aChannel), std::string(lInvPolarity.at(aChannel.getIndex()) ? "Inverted" : "Normal"));

  // Set state
  if (lEnabled.at(aChannel.getIndex()))
    set<std::string>(*mPropertyState.at(&aChannel), "Enabled");
  else
    set<std::string>(*mPropertyState.at(&aChannel), "Disabled");
}


void FireflyRx16G::retrieveMetricValues()
{
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  setMetric(mMetricTemperature, measurePhysicalQuantity(lFirefly, "Temperature").first);
  setMetric(mMetricPeakTemperature, measurePhysicalQuantity(lFirefly, "Peak Temperature").first);

  // Don't continue if all channels are disabled
  if (std::all_of(getInputs().cbegin(), getInputs().cend(),
                  [](auto channel) { return channel->getMonitoringStatus() == core::monitoring::kDisabled; })) {
    return;
  }

  const auto lLOS = measureChannelFlag(lFirefly, "LOS alarms", kChannels, kAlarmValueMap);

  for (auto* channel : getInputs()) {
    if (channel->getMonitoringStatus() == core::monitoring::kDisabled) {
      continue;
    }
    setMetric(*mMetricLOS.at(channel), lLOS.at(channel->getIndex()));
  }
}


void FireflyRx16G::retrieveInputMetricValues(Channel& aChannel)
{
  // Monitoring data for input channels updated by parent instead (since called delegateUpdateMetrics in CTOR)
}

} // namespace swatch
} // namespace serenity