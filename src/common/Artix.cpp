
#include "serenity/swatch/Artix.hpp"


#include "uhal/ConnectionManager.hpp"

#include "swatch/core/Factory.hpp"

#include "serenity/LocationInfo.hpp"
#include "serenity/swatch/commands/ResetArtixTTC.hpp"
#include "serenity/swatch/utilities.hpp"


SWATCH_REGISTER_CLASS(serenity::swatch::Artix)


namespace serenity {
namespace swatch {

using namespace ::swatch;

const std::string Artix::CmdIds::kReset = "reset";


Artix::Artix(const core::AbstractStub& aStub) :
  ServiceModule(aStub)
{
  uhal::setLogLevelTo(uhal::Notice());
  mController.reset(new ArtixTTCController(uhal::ConnectionManager::getDevice(getStub().id, getStub().uri, getStub().addressTable)));

  registerCommand<commands::ResetArtixTTC>(CmdIds::kReset);
}


Artix::~Artix()
{
}


ArtixTTCController& Artix::getController()
{
  return *mController;
}


void Artix::retrieveMetricValues()
{
  const auto lLocationInfo = getBoardLocation();
  setMetric(mMetricLogicalSlot, uint16_t(lLocationInfo.getLogicalSlot()));
  setMetric(mMetricShelfAddress, lLocationInfo.getShelfAddress());
}


} // namespace swatch
} // namespace serenity
