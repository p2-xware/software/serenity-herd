
#include "serenity/swatch/Processor.hpp"


#include <boost/lexical_cast.hpp>

#include <log4cplus/loggingmacros.h>

#include "swatch/core/Factory.hpp"

#include "SMASH/core/Smash.hpp"

#include "serenity/power.hpp"


SWATCH_REGISTER_CLASS(serenity::swatch::S1Processor)
SWATCH_REGISTER_CLASS(serenity::swatch::Z1Processor)


namespace serenity {
namespace swatch {

using namespace ::swatch;

namespace {

phase2::DeviceStub setDeviceURI(const phase2::DeviceStub& aStub, const std::string& aURI)
{
  if (not aStub.uri.empty())
    throw std::runtime_error("Please remove the URI '" + aURI + "' from the Herd config file / subsystem plugin. It's being automatically set to '" + aURI + "' by the Serneity plugin");

  phase2::DeviceStub lStub(aStub);
  lStub.uri = aURI;
  return lStub;
}

} // namespace (unnamed)


Processor<BoardSeries::S1>::Processor(const swatch::core::AbstractStub& aStub) :
  BaseProcessor(
      setDeviceURI(dynamic_cast<const phase2::DeviceStub&>(aStub), "ipbusmmap-2.0:///dev/uio-ipbus-remote"),
      ::serenity::getIOChannelMap(getBoardType(), boost::none, boost::none),
      128, // FIXME: Add support for board with VU9P
      TypeCarrier<BoardSeries::S1>()),
  mPowerMetric(registerMetric<float>("power", "Power"))
{
  // Check that using the right Herd config file for the board series!
  if (getBoardSeries() != BoardSeries::S1)
    throw std::runtime_error("Cannot use Serenity S1 Processor specialization on a Serenity Z1");
  if (aStub.id != "fpga")
    throw std::runtime_error("Invalid processor ID, '" + aStub.id + "', for a Serenity Z1 (expected 'fpga')");

  // Refuse to run on S1.3 until we run some dedicated tests on there
  if (getBoardType() == BoardType::S1_3)
    throw std::runtime_error("Serenity Herd plugin doesn't support the S1.3 quite yet");

  // Monitoring data
  mPowerMetric.setUnit("W");
  mPowerMetric.setFormat(core::format::kFixedPoint, 1);
}


Processor<BoardSeries::S1>::~Processor()
{
}


void Processor<BoardSeries::S1>::retrievePropertyValues()
{
  // To be filled in ...

  emp::swatch::Processor::retrievePropertyValues();
}


void Processor<BoardSeries::S1>::retrieveMetricValues()
{
  std::unique_ptr<Smash> lSmash(startSmashSession());
  const auto lPower = getPowerSummary(*lSmash, boost::none);
  setMetric(mPowerMetric, lPower.fpgaLogic + lPower.fpgaMGTs);

  emp::swatch::Processor::retrieveMetricValues();
}


Processor<BoardSeries::Z1>::Processor(const swatch::core::AbstractStub& aStub) :
  BaseProcessor(
      aStub,
      ::serenity::getIOChannelMap(getBoardType(), getDaughterCardType(Site::X0), getDaughterCardType(Site::X1)),
      getNumberOfPorts(aStub.id == "x0" ? Site::X0 : Site::X1),
      TypeCarrier<BoardSeries::Z1>()),
  mSite(aStub.id == "x0" ? Site::X0 : Site::X1),
  mDCType((addPropertyGroup("Hardware info"), registerProperty<std::string>("Type", "Hardware info"))),
  mDCUID(registerProperty<std::string>("UID", "Hardware info")),
  mPowerMetric(NULL)
{
  // Check that using the right Herd config file for the board series!
  if (getBoardSeries() != BoardSeries::Z1)
    throw std::runtime_error("Cannot use Serenity Z1 Processor specialization on a Serenity S1");
  if ((aStub.id != "x0") and (aStub.id != "x1"))
    throw std::runtime_error("Invalid processor ID, '" + aStub.id + "', for a Serenity Z1 (expected x0 or x1)");

  // Monitoring data (allow for user to stop power measurements, since causes FPGA
  //   to become unprogrammed on HNZQ)
  if (std::getenv("SERENITY_SWATCH_NO_DC_POWER_METRIC") != NULL) {
    log4cplus::Logger lLogger(log4cplus::Logger::getInstance("serenity.swatch.Processor"));
    LOG4CPLUS_WARN(lLogger, "Skipping power measurements since 'SERENITY_SWATCH_NO_DC_POWER_METRIC' variable defined");
  }
  else {
    mPowerMetric = &registerMetric<float>("power", "Power");
    mPowerMetric->setUnit("W");
    mPowerMetric->setFormat(core::format::kFixedPoint, 1);
  }
}


Processor<BoardSeries::Z1>::~Processor()
{
}


Site Processor<BoardSeries::Z1>::getSite() const
{
  return mSite;
}


void Processor<BoardSeries::Z1>::retrievePropertyValues()
{
  std::unique_ptr<Smash> lSmash(startSmashSession());

  std::ostringstream lType;
  lType << getDCType(mSite);
  set(mDCType, lType.str());

  // TODO: Set mDCUID when SMASH bug fixed ("Port 'EepromSt8functionIFvhRKSt6vectorIhSaIhEERS2_EE' not found in Port List")

  emp::swatch::Processor::retrievePropertyValues();
}


void Processor<BoardSeries::Z1>::retrieveMetricValues()
{
  if (mPowerMetric != NULL) {
    std::unique_ptr<Smash> lSmash(startSmashSession());
    const auto lPower = getPowerSummary(*lSmash, mSite);
    setMetric(*mPowerMetric, lPower.fpgaLogic + lPower.fpgaMGTs);
  }

  emp::swatch::Processor::retrieveMetricValues();
}


DaughterCardType Processor<BoardSeries::Z1>::getDCType(const Site aSite)
{
  const auto lResult = getDaughterCardType(aSite);

  // If daughter card absent then throw 'ProcessorAbsent' exception to declare this to framework
  if (lResult == boost::none)
    throw ::swatch::phase2::ProcessorAbsent("No daughter card is situated at " + boost::lexical_cast<std::string>(aSite));

  return *lResult;
}


size_t Processor<BoardSeries::Z1>::getNumberOfPorts(const Site aSite)
{
  const auto lDCType = getDCType(aSite);

  const auto lIt = kPortCountMap.find(lDCType);
  if (lIt != kPortCountMap.end())
    return lIt->second;
  else
    throw std::runtime_error("Could not determine number of I/O ports for daughter card " + boost::lexical_cast<std::string>(lDCType));
}


const std::map<DaughterCardType, size_t> Processor<BoardSeries::Z1>::kPortCountMap = {
  { DaughterCardType::KU15P_SM1_v1, 72 },
  { DaughterCardType::KU15P_SM1_v2, 72 },
  { DaughterCardType::KU15P_SO2, 72 },
  { DaughterCardType::VU9P_SO2, 120 },
  { DaughterCardType::VU13P_SO2, 128 },
  { DaughterCardType::VU7P_SO1_v1_1, 72 }
};

} // namespace swatch
} // namespace serenity
