#include "serenity/swatch/FireflyTx16G.hpp"

#include "SMASH/core/Element.hpp"
#include "SMASH/core/Smash.hpp"

#include "serenity/board.hpp"
#include "serenity/fireflys.hpp"
#include "serenity/swatch/utilities.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace serenity {
namespace swatch {

const std::vector<size_t> FireflyTx16G::kChannels { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

// Firefly Tx class for both 16G and CERN-B parts

FireflyTx16G::FireflyTx16G(const std::string& aId, const std::string& aAlias, const ::Element& aFirefly) :
  FireflyBase(aId, aAlias, aFirefly, {}, kChannels),
  mMetricVoltage(NULL)
{
  if ((getBoardSeries() == BoardSeries::S1) and (getBoardType() != BoardType::S1_1)) {
    mMetricVoltage = &registerMetric<float>("voltage", "Voltage", ::swatch::core::GreaterThanCondition<float>(3.3));
    mMetricVoltage->setUnit("V");
    mMetricVoltage->setFormat(::swatch::core::format::kFixedPoint, 1);
  }

  for (auto* channel : getOutputs()) {
    // Properties
    mPropertyState[channel] = &channel->registerProperty<std::string>("State");
    mPropertyPolarity[channel] = &channel->registerProperty<std::string>("Polarity");

    // Metrics
    mMetricLaserFault[channel] = &channel->registerMetric<bool>("laserFault", "Laser fault", ::swatch::core::EqualCondition<bool>(true));

    // Declare that this monitorable object will update each of the above per-channel metrics
    channel->delegateMetricUpdate(mMetricLaserFault.at(channel)->getId(), *this);
  }
}


FireflyTx16G::~FireflyTx16G()
{
}


void FireflyTx16G::retrieveMetricValues()
{
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  setMetric(mMetricTemperature, measurePhysicalQuantity(lFirefly, "Temperature").first);
  setMetric(mMetricPeakTemperature, measurePhysicalQuantity(lFirefly, "Peak Temperature").first);

  if (mMetricVoltage) {
    const auto lVoltage = readFireflyVoltage(*lSmash, mElementName);
    setMetric(*mMetricVoltage, float(lVoltage == kFirefly3V3 ? 3.3 : 3.75));
  }

  // Don't continue if all channels are disabled
  if (std::all_of(getOutputs().cbegin(), getOutputs().cend(),
                  [](auto channel) { return channel->getMonitoringStatus() == ::swatch::core::monitoring::kDisabled; })) {
    return;
  }

  const auto lLaserFault = measureChannelFlag(lFirefly, "Laser fault alarms", kChannels, kAlarmValueMap);

  for (auto* channel : getOutputs()) {
    if (channel->getMonitoringStatus() == ::swatch::core::monitoring::kDisabled) {
      continue;
    }
    setMetric(*mMetricLaserFault.at(channel), lLaserFault.at(channel->getIndex()));
  }
}


void FireflyTx16G::retrieveOutputPropertyValues(Channel& aChannel)
{
  std::unique_ptr<Smash> lSmash(startSmashSession());
  auto& lFirefly(*lSmash->GetElement(mElementName));

  const auto lEnabled = measureChannelFlag(lFirefly, "Channel", kChannels, kEnableValueMap);
  const auto lInvPolarity = measureChannelFlag(lFirefly, "Polarity", kChannels, kPolarityInvertedValueMap);

  // Set state
  if (lEnabled.at(aChannel.getIndex()))
    set<std::string>(*mPropertyState.at(&aChannel), "Enabled");
  else
    set<std::string>(*mPropertyState.at(&aChannel), "Disabled");

  set<std::string>(*mPropertyPolarity.at(&aChannel), std::string(lInvPolarity.at(aChannel.getIndex()) ? "Inverted" : "Normal"));
}


void FireflyTx16G::retrieveOutputMetricValues(Channel& aChannel)
{
  // Monitoring data for output channels updated by parent instead (since called delegateUpdateMetrics in CTOR)
}

} // namespace swatch
} // namespace serenity