
#include "serenity/swatch/utilities.hpp"


#include <algorithm>
#include <cstddef>
#include <iostream>
#include <list>
#include <mutex>
#include <thread>

#include <boost/filesystem.hpp>

#include <log4cplus/loggingmacros.h>

#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/parse.h>
#include <yaml-cpp/yaml.h>

#include "SMASH/core/Element.hpp"
#include "SMASH/core/Smash.hpp"

#include "swatch/core/utilities.hpp"

namespace serenity {
namespace swatch {

using namespace std::literals;
using namespace ::swatch;

const std::string getFireflyFirmwareVersion(Element& lElement, const std::string lName)
{
  log4cplus::Logger lLogger(log4cplus::Logger::getInstance("serenity.swatch"));
  for (size_t j = 1;; j++) {
    try {
      const std::string knownFirmwareVersion = lElement.Measure("Firmware Version");
      LOG4CPLUS_DEBUG(lLogger, "Read Firefly firmware version for " << lName << ": " << knownFirmwareVersion);
      return knownFirmwareVersion;
    }
    catch (const std::exception& e) {
      if (j == 3) {
        const std::string unknownFirmwareVersion = "Unknown";
        LOG4CPLUS_INFO(lLogger, "Failed to read Firefly firmware version for " << lName);
        return unknownFirmwareVersion;
      }
      LOG4CPLUS_INFO(lLogger, "Exception thrown when measuring FW version: " << lName << " (" << core::demangleName(typeid(e).name()) << ", \"" << e.what() << "\"). Retrying (attempt " << std::to_string(j + 1) << ").");
    }
  }
}


const std::string getFireflyBatch(const std::string& firmwareVersion)
{
  std::map<std::string, std::string> kFireflyBatchMap {
    { "0x000F08", "4+4" },
    { "0x000F0C", "4+4" },
    { "0x00120215", "alpha-1" },
    { "0x0013021A", "alpha-1" },
    { "0x00120606", "alpha-2" },
    { "0x00130A01", "alpha-2" },
    { "0x00130C03", "beta-1" }
  };

  std::string lBatch = kFireflyBatchMap.count(firmwareVersion) ? kFireflyBatchMap[firmwareVersion] : "unknown batch";
  return lBatch;
}

} // namespace swatch
} // namespace serenity
