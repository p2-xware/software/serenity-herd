# Serenity SWATCH plugin

This repository contains a SWATCH plugin library for the Serenity board. This plugin registers control/configuration procedures and eventually monitoring data to the SWATCH framework, by wrapping short sections of code that invoke functions from the core Serenity control/monitoring libraries (see below for links).

The Serenity plugin builds on top of the EMP SWATCH plugin, which defines commands for controlling the EMP (Extensible, Modular, data Processor) firmware framework that runs on the Serenity's main data-processing FPGAs.


## Dependencies

The plugin library has five main dependencies:
 * [SWATCH](https://gitlab.cern.ch/cms-cactus/core/swatch)
 * [EMP software](http://serenity.web.cern.ch/serenity/emp-fwk/software/)
 * [EMP SWATCH plugin](https://gitlab.cern.ch/p2-xware/software/emp-herd/)
 * [SMASH](https://gitlab.cern.ch/p2-xware/software/smash)
 * [Serenity toolbox](https://gitlab.cern.ch/p2-xware/software/serenity-toolbox)


## Build instructions

To build the plugin, firstly clone this repository:
```sh
git clone ssh://git@gitlab.cern.ch:7999/p2-xware/software/serenity-herd.git
```

The easiest way to build the plugin is inside a docker container, using the
[VSCode Dev Containers extension](https://code.visualstudio.com/docs/devcontainers/containers).
This repository includes a VSCode configuration file (`.devcontainer/devcontainer.json`) which
allows one to build in a development container that already contains all dependencies, along with
relevant tooling like git. To use this setup you just need to open the path for the cloned
repository in VSCode, click on the green bar in the bottom left of the window and then click
"Reopen in container". Instructions for other development environments can be found
[here](https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/development-environment.html).

After VSCode has downloaded & started the development container (or other development environment set
up), to build the plugin simply follow the standard CMake build workflow in the VScode terminal:
```sh
mkdir build
cd build
cmake3 ..
make -j$(nproc)
```


## Deployment

This repository's CI pipeline automatically builds a container image containing this plugin along
with all dependencies, and uploads it to `gitlab-registry.cern.ch` using the following URL scheme:
 * Tags: `gitlab-registry.cern.ch/p2-xware/software/serenity-herd/centos7/herd:vX.Y.Z`
 * Branches: `gitlab-registry.cern.ch/p2-xware/software/serenity-herd/centos7/herd:BRANCHNAME-COMMITSHA`
   (where `COMMITSHA` is the first 8 characters of the git commit's SHA)

The simplest way to run the plugin on a Serenity is to run this image. Notably the image must
always be run using the `/opt/cactus/bin/serenity/docker-run.sh` script (which wraps `docker run`,
adding extra arguments to e.g. make device files accessible inside the container). E.g. for the
image from commit `1234abcd` on the `master` branch:
```sh
/opt/cactus/bin/serenity/docker-run.sh -d --name herd -p 3000:3000 gitlab-registry.cern.ch/p2-xware/software/serenity-herd/centos7/herd:master-1234abcd
``` 

Instructions on how to deploy the corresponding off-board software (Shep) - and use it to control
and monitor boards - can be found in the [ShepHERD user guide](https://cms-l1t-phase2.docs.cern.ch/online-sw/user-guide/index.html)
