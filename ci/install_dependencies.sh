#!/bin/bash

set -e
set -x


SMASH_VERSION=1.0.2
#SERENITY_VERSION=v0.4.0
SERENITY_COMMIT=f0e5668b


if [ "$1" == "app" ]
then
  yum -y install pugixml libX11 imlib2 libftdi pciutils libicu boost-date-time boost-chrono boost-regex
  yum clean all

  mkdir -p /etc/serenity
  echo 'Run /opt/smash/etc/serenity/configuration/BareZ1p2.smash' > /etc/serenity/board.smash

else
  ORIGINAL_DIR=$(pwd)
  echo "nproc: $(nproc)"

  # 1) Install other dependencies of SMASH and Serenity toolbox

  # Fix for Alma8 default compiler version 
  OS_NAME=$(grep  "^NAME=" /etc/os-release | cut -d= -f2 | tr -d '"')
  echo $OS_NAME
  if [[ "$OS_NAME" == "CentOS Linux" ]]; then
    SMASH_REPO=ci/smash_centos7.repo
    PYTHON_DEVEL_RPM=python36-devel
  elif [[ "$OS_NAME" == "AlmaLinux" ]]; then
    SMASH_REPO=ci/smash.repo
    PYTHON_DEVEL_RPM=python3.11-devel
  else
    echo "ERROR: Unknown operating system version"
    exit 1
  fi

  cp ${SMASH_REPO} /etc/yum.repos.d/
  yum -y install --exclude *debuginfo \
    ${PYTHON_DEVEL_RPM} \
    pugixml-devel libX11-devel imlib2 imlib2-devel libftdi-devel pciutils-devel \
    git-core rpm-build

  yum -y install --exclude *debuginfo \
      smash-*-${SMASH_VERSION}

  yum clean all


  # 2a) Build Serenity toolbox
  if [[ -z $SERENITY_VERSION && -z $SERENITY_COMMIT ]]; then
    echo "ERROR: Either SERENITY_VERSION or SERENITY_COMMIT must be set"
    exit 1
  elif [[ -n $SERENITY_VERSION && -n $SERENITY_COMMIT ]]; then
    echo "ERROR: One one of SERENITY_VERSION and SERENITY_COMMIT can be set"
    exit 1
  fi

  SERENITY_TOOLBOX_SOURCE_DIR=/tmp/________________________/serenity-toolbox
  mkdir -p $(dirname ${SERENITY_TOOLBOX_SOURCE_DIR})
  git clone --recurse-submodules https://user:glpat-wPtVZquApUfP_4RBihP2@gitlab.cern.ch/p2-xware/software/serenity-toolbox.git ${SERENITY_VERSION:+-b ${SERENITY_VERSION}} ${SERENITY_TOOLBOX_SOURCE_DIR}
  cd ${SERENITY_TOOLBOX_SOURCE_DIR}
  if [[ -n $SERENITY_COMMIT ]]; then
    git checkout $SERENITY_COMMIT
  fi

  make -j$(nproc)
  make rpm

  # 2b) Install Serenity toolbox
  rpm -iv --ignoresize --nodeps $(find $(pwd) -name "*.rpm" | grep -v debuginfo)
fi

cd ${ORIGINAL_DIR}
rm -rf ${EMP_TOOLBOX_SOURCE_DIR} ${SMASH_SOURCE_DIR} ${SERENITY_TOOLBOX_SOURCE_DIR}
